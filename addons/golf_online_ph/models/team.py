from odoo import models, fields, api
from odoo.exceptions import ValidationError

class GolfTeam(models.Model):
    _name = "goph.golf_team"
    _rec_name = "team_name"

    team_name = fields.Char(required=True)
    tournament_id = fields.Many2one("goph.tournament", ondelete="cascade", string="Tournament")
    team_members = fields.Many2many("goph.golf_player", string="Players")
    tournament_scoring_format = fields.Selection(related="tournament_id.tournament_scoring_format")

    # computed fields
    team_handicap = fields.Float(digits=(6, 2), compute="_compute_handicap", string="Team Handicap", default=0.0)

    main_division_id = fields.Many2one("goph.eligible_division_instance")
    team_classification_id = fields.Many2one("goph.classification", compute="_set_classification")
    stored_team_classification_id = fields.Many2one("goph.classification", store=True, string="Classification")

    active_team_members = fields.One2many("goph.registered_player", "team_id")
    team_total_gross_score = fields.Integer(string="Gross Score", store=True)
    team_total_net_score = fields.Integer(string="Net Score", store=True)

    #  ranking
    team_gross_ranking = fields.Integer(string="Gross Ranking", store=True)
    team_net_ranking = fields.Integer(string="Net Ranking", store=True)
    team_overall_gross_ranking = fields.Integer(string="Overall Gross Ranking", store=True)
    team_overall_net_ranking = fields.Integer(string="Overall Net Ranking", store=True)

    team_rounds = fields.One2many("goph.team_rounds", "team_id", string="Rounds")

    @api.multi
    @api.constrains('team_members')
    def _check_number_of_members(self):
        for record in self:
            members_count = len(record.team_members)
            if members_count > record.tournament_id.max_team_members or members_count < record.tournament_id.min_team_members:
                raise ValidationError("Lacking or Too Many Members in a Team!")

    @api.multi
    def write(self,vals):
        players_list = []
        # import pdb; pdb.set_trace()
        if 'team_members' in vals:
            players_list = vals.get('team_members')
        for rec in self:
            for player in players_list:
                original_player_ids = rec.team_members.ids

                if player[0] == 6:
                    to_retain_ids = player[2]
                    to_remove_ids = set(to_retain_ids).symmetric_difference(set(original_player_ids))
                    rec.env['goph.registered_player'].search([
                        ('tournament_id', '=', rec.tournament_id.id),
                        ('player_id', 'in', list(to_remove_ids)),
                        ('team_id', '=', rec.id)]).unlink()

                    registered_player_obj = self.env['goph.registered_player']

                    for retain_id in to_retain_ids:
                        if retain_id in original_player_ids:
                            continue
                        golf_player = self.env['goph.golf_player'].browse(retain_id)
                        if rec.tournament_id.tournament_scoring_format == 'system-36':
                            handicap = 0
                        else:
                            handicap = golf_player.player_handicap
                        registered_player_obj.create({
                            'player_id': retain_id,
                            'tournament_id': rec.tournament_id.id,
                            'team_id': rec.id,
                            'player_handicap': handicap,
                            'main_division_id': rec.main_division_id.id
                        })
            
            old_team_main_division = rec.main_division_id
            old_team_classification = rec.stored_team_classification_id

            super(GolfTeam, rec).write(vals)
            # if main division has been changed
            if 'main_division_id' in vals:
                # assigning new main division after write
                new_main_division = rec.main_division_id
                # re-compute new classification by calling 'team_classification_id'
                new_classification = rec.team_classification_id
                
                # update main division and classification of each member
                for player in rec.active_team_members:
                    player.write({'main_division_id': rec.main_division_id.id, 'player_classification_id': new_classification.id })

                # recompute awardees
                old_team_main_division.update_awardees(old_team_main_division)
                old_team_classification.update_awardees(old_team_classification)
                new_main_division.update_awardees(new_main_division)
                new_classification.update_awardees(new_classification)

        return True

    @api.multi
    @api.depends('team_handicap')
    def _set_classification(self):
        for record in self:
            classifications = record.main_division_id.classifications
            for classification in classifications:
                if ((record.team_handicap >= classification.classification_min_handicap)
                        and (record.team_handicap <= classification.classification_max_handicap)):
                    record.team_classification_id = classification.id
                    record.write({'stored_team_classification_id' : classification.id})
                    break

            for member in record.active_team_members:
                member.player_classification_id = record.stored_team_classification_id
                member.write({'stored_player_classification_id': record.stored_team_classification_id.id })
    
    @api.multi
    def _compute_handicap(self):
        for record in self:
            handicap_calc = record.tournament_id.tournament_handicap_calculator
            total_handicap = 0
            handicap_average = 0

            for member in record.active_team_members:
                total_handicap += member.player_handicap    
            # import pdb; pdb.set_trace()
            if len(record.active_team_members) != 0:
                handicap_average = total_handicap / len(record.active_team_members)
                
            if handicap_calc == 'ave-mult':
                handicap_average = handicap_average * (record.tournament_id.custom_handicap_multiplier / 100)

            record.team_handicap = handicap_average

    def update_team_score(self, team):
        vals = {}
        total_gross_score = 0
        total_net_score = 0

        for round_score in team.team_rounds:
            total_gross_score += round_score.round_total_gross_score
            total_net_score += round_score.round_total_net_score

        vals['team_total_gross_score'] = total_gross_score
        vals['team_total_net_score'] = total_net_score
        
        team.write(vals)
        team.update_main_division_rankings(team)

    def update_main_division_rankings(self, registered_team):
        
        same_main_division_teams = registered_team.main_division_id.qualified_teams
        teams_with_zero_gross = same_main_division_teams.filtered(lambda x : x.team_total_gross_score == 0)
        teams_with_gross_score = same_main_division_teams.filtered(lambda x : x.team_total_gross_score != 0)

        for team in same_main_division_teams:
            
            vals = {}

            if team.team_total_gross_score == 0:
                sorted_zero_gross = teams_with_zero_gross.sorted('id').ids
                vals['team_overall_gross_ranking'] = sorted_zero_gross.index(team.id) + 1 + len(teams_with_gross_score)
                vals['team_overall_net_ranking'] = vals['team_overall_gross_ranking']
            else:
                if (registered_team.tournament_id.tournament_scoring_format == 'system-36' or 
                        registered_team.tournament_id.tournament_scoring_format == 'stroke-play'):
                    sorted_by_gross_overall = teams_with_gross_score.sorted('team_total_gross_score').ids
                    sorted_by_net_overall = teams_with_gross_score.sorted('team_total_net_score').ids
                else:
                    sorted_by_gross_overall = teams_with_gross_score.sorted('team_total_gross_score', reverse=True).ids
                    sorted_by_net_overall = teams_with_gross_score.sorted('team_total_net_score', reverse=True).ids

                vals['team_overall_gross_ranking'] = sorted_by_gross_overall.index(team.id) + 1
                vals['team_overall_net_ranking'] = sorted_by_net_overall.index(team.id) + 1

            team.write(vals)

        # update classification rankings if tournament format is not system-36
        if registered_team.tournament_id.tournament_scoring_format != 'system-36':
            registered_team.update_classification_ranking(registered_team)
    
    def update_classification_ranking(self, registered_team):
        
        same_classification_teams = registered_team.stored_team_classification_id.qualified_teams
        teams_with_zero_gross = same_classification_teams.filtered(lambda x : x.team_total_gross_score == 0)
        teams_with_gross_score = same_classification_teams.filtered(lambda x : x.team_total_gross_score != 0)

        for team in same_classification_teams:
            
            vals = {}

            if team.team_total_gross_score == 0:
                sorted_zero_gross = teams_with_zero_gross.sorted('id').ids
                vals['team_overall_gross_ranking'] = sorted_zero_gross.index(team.id) + 1 + len(teams_with_gross_score)
                vals['team_overall_net_ranking'] = vals['team_overall_gross_ranking']
            else:
                if (registered_team.tournament_id.tournament_scoring_format == 'system-36' or 
                        registered_team.tournament_id.tournament_scoring_format == 'stroke-play'):
                    sorted_by_gross = teams_with_gross_score.sorted('team_total_gross_score').ids
                    sorted_by_net = teams_with_gross_score.sorted('team_total_net_score').ids
                else:
                    sorted_by_gross = teams_with_gross_score.sorted('team_total_gross_score', reverse=True).ids
                    sorted_by_net = teams_with_gross_score.sorted('team_total_net_score', reverse=True).ids

                vals['team_gross_ranking'] = sorted_by_gross.index(team.id) + 1
                vals['team_net_ranking'] = sorted_by_net.index(team.id) + 1

            team.write(vals)

    def update_classification(self, team):
        classifications = team.main_division_id.classifications
        for classification in classifications:
            if ((team.team_handicap >= classification.classification_min_handicap)
                    and (team.team_handicap <= classification.classification_max_handicap)):
                team.team_classification_id = classification.id
                team.write({'stored_team_classification_id' : classification.id})
                break

        for member in team.active_team_members:
            member.player_classification_id = team.stored_team_classification_id
            member.write({'stored_player_classification_id': team.stored_team_classification_id.id })


class TeamRounds(models.Model):
    _name = "goph.team_rounds"
    _rec_name = "round_id"

    round_id = fields.Many2one("goph.tournament_rounds", ondelete="cascade")
    team_id = fields.Many2one("goph.golf_team", ondelete="cascade")
    round_total_gross_score = fields.Integer(string="Round Gross Score", store=True)
    round_total_net_score = fields.Integer(string="Round Net Score", store=True)

    score_cards = fields.One2many("goph.player_score_card", "team_round_id", compute="_get_score_cards")

    def update_round_score(self, team, round_id):
        vals = {}
        round_gross_score = 0
        round_net_score = 0

        round_players = round_id.round_players.filtered(lambda r: r.team_id.id == team.id)
        
        top_gross_players = round_players.sorted('total_gross_score', reverse=True)
        top_gross_players = top_gross_players[:team.tournament_id.number_of_scorers_counted]

        top_net_players = round_players.sorted('total_net_score', reverse=True)
        top_net_players = top_net_players[:team.tournament_id.number_of_scorers_counted]

        for player in top_gross_players:
            round_score_card = player.player_score_cards.filtered(lambda r: r.round_id.id == round_id.id)
            round_gross_score += round_score_card.stored_summary_gross_points
        
        for player in top_net_players:
            round_score_card = player.player_score_cards.filtered(lambda r: r.round_id.id == round_id.id)
            round_net_score += round_score_card.stored_summary_net_points

        vals['round_total_gross_score'] = round_gross_score
        vals['round_total_net_score'] = round_net_score

        current_team_round = team.team_rounds.filtered(lambda r: r.round_id.id == round_id.id)

        current_team_round.write(vals)
        team.update_team_score(team)

    @api.multi
    def _get_score_cards(self):
        for record in self:
            record.score_cards = self.env['goph.player_score_card'].search([
                    ('team_id', '=', record.team_id.id),
                    ('round_id', '=', record.round_id.id)
                ])
            