from odoo import models, fields, api
import datetime
import random
from odoo.exceptions import ValidationError


class Flight(models.Model):
    _name = "goph.tournament_flights"
    _rec_name = "flight_name"

    round_id = fields.Many2one("goph.tournament_rounds", string="Round", ondelete="cascade")
    tournament_id = fields.Many2one(related="round_id.tournament_id")
    flight_name = fields.Char()
    flight_schedules = fields.One2many("goph.tournament_flight_schedule", "flight_id", string="Schedule")

    @api.constrains('flight_schedules')
    def _check_flight_players(self):
        for record in self:
            for schedule in record.flight_schedules:
                current_player_ids = schedule.flight_players.ids

                player_ids_from_other_flights = []
                player_ids_from_other_schedules = []

                other_flights_obj = self.env['goph.tournament_flights'].search([
                    ('id', '!=', record.id),
                ])

                flight_schedule_obj = self.env['goph.tournament_flight_schedule'].search([
                    ('id', '!=', schedule.id)
                ])

                for other_flights in other_flights_obj:
                    for other_flight_schedule in other_flights.flight_schedules:
                        for other_player in other_flight_schedule.flight_players:
                            player_ids_from_other_flights.append(
                                other_player.id)

                for other_flight_schedule in flight_schedule_obj:
                    for player in other_flight_schedule.flight_players:
                        player_ids_from_other_schedules.append(player.id)

                if [i for i in current_player_ids if i in player_ids_from_other_flights]:
                    raise ValidationError(
                        "Player is already scheduled on another Flight")

                if [i for i in current_player_ids if i in player_ids_from_other_schedules]:
                    raise ValidationError(
                        "Player is already scheduled on this Flight")

    @api.model
    def create(self, vals):
        flight_schedule = self.env['goph.tournament_flight_schedule']
        
        new_obj = super(Flight, self).create(vals)

        round_players_ids = new_obj.round_id.round_players.ids
        tee_of_time_start = new_obj.tournament_id.tee_of_time_start
        tee_of_time_end = new_obj.tournament_id.tee_of_time_end
        scheduling_interval = new_obj.tournament_id.scheduling_interval
        players_per_time = new_obj.tournament_id.players_per_time
        
        if new_obj.tournament_id.tournament_tee_of_format == 'shotgun':
            while round_players_ids:
                
                if players_per_time > len(round_players_ids):
                    players_per_time = len(round_players_ids)

                # getting random players from round players
                players_to_be_assigned_ids = random.sample(round_players_ids, players_per_time)
                
                # converting float time to readable string time
                tee_of_time_start_str = new_obj.convert_float_time_to_string(tee_of_time_start)

                created_flight_sched = flight_schedule.create({
                        'flight_id' : new_obj.id,
                        'tournament_id': new_obj.tournament_id.id,
                        'sched_time': tee_of_time_start,
                        'sched_time_str': tee_of_time_start_str,
                        'flight_players': [(6, 0, players_to_be_assigned_ids)]
                    })
                
                # update round players
                for player_id in players_to_be_assigned_ids:
                    round_players_ids.remove(player_id)
                # update tee_of_time_start
                tee_of_time_start += ((scheduling_interval / 100) / 0.6)
        return new_obj
    
    def convert_float_time_to_string(self, float_time):

        str_time = ""

        equivalent_hour = int(float_time)

        # getting equivalent minutes
        decimal_point_number = float(str(float_time-int(float_time))[1:])
        equivalent_minutes = int(round(decimal_point_number * 60))
        
        if equivalent_minutes == 0:
            str_equivalent_minutes = "00"
        elif equivalent_minutes < 10:
            str_equivalent_minutes = "0"+str(equivalent_minutes)
        else:
            str_equivalent_minutes = str(equivalent_minutes)
            
        if equivalent_hour < 12:  # AM
            if equivalent_hour < 10:
                str_equivalent_hour = "0" + str(equivalent_hour)
            else:
                str_equivalent_hour = str(equivalent_hour)

            str_time = str_equivalent_hour + ":" + str_equivalent_minutes + " AM"
        else:  # PM
            if equivalent_hour == 12:
                equivalent_hour = 12
            else:
                equivalent_hour = equivalent_hour - 12
            
            if equivalent_hour < 10:
                str_equivalent_hour = "0" + str(equivalent_hour)
            else:
                str_equivalent_hour = str(equivalent_hour)

            str_time = str_equivalent_hour + ":" + str_equivalent_minutes + " PM"
        
        return str_time



class FlightPlayers(models.Model):
    _name = "goph.tournament_flight_schedule"
    
    flight_id = fields.Many2one("goph.tournament_flights", string="Flight", ondelete="cascade")
    tournament_id = fields.Many2one("goph.tournament")
    sched_time = fields.Float(string="Time")
    sched_time_str = fields.Char(string="Time")
    flight_players = fields.Many2many("goph.registered_player", string="Players")
