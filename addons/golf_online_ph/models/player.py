from odoo import models, fields, api
from datetime import date
import dateutil.relativedelta as relativedelta


class GolfPlayer(models.Model):
    _name = "goph.golf_player"
    _rec_name = "player_name"

    player_image = fields.Binary(string="Player Image")
    player_first_name = fields.Char(string="First Name")
    player_name = fields.Char(string="Last Name")
    player_gender = fields.Selection([("male", "Male"),
                                      ("female", "Female")],
                                     string="Gender",
                                     required=True)
    player_birthday = fields.Date(string="Birthday")
    player_age = fields.Integer(string="Age", compute="_compute_age")
    player_club_membership = fields.Char(string="Club Membership")
    player_club_membership_id = fields.Char(string="Club Membership ID")
    player_eligible_division = fields.Selection([("division1", "Senior"),
                                                 ("division2", "Regular"),
                                                 ("division3", "Ladies")],
                                                string="Eligible Division",
                                                default="division2",
                                                required=True)
    player_handicap = fields.Float(string="Handicap")

    # for player history
    team_ids = fields.Many2many("goph.golf_team", string="Team")
    associated_tournaments = fields.Integer(compute="_get_tournaments")
    registration_instances = fields.One2many("goph.registered_player", "player_id")
    score_cards_history = fields.One2many(related="registration_instances.player_score_cards")

    @api.multi
    @api.depends('player_birthday')
    def _compute_age(self):
        for record in self:
            if record.player_birthday and record.player_birthday <= fields.Date.today():
                record.player_age = relativedelta.relativedelta(
                    fields.Date.from_string(fields.Date.today()),
                    fields.Date.from_string(record.player_birthday)).years
            else:
                record.player_age = 0

    @api.multi
    def _get_tournaments(self):
        for record in self:
            joined_tournaments = self.env['goph.registered_player'].search([
                ('player_id', '=', record.id)
            ])
            record.associated_tournaments = len(joined_tournaments)


class RegisteredPlayer(models.Model):
    _name = "goph.registered_player"
    _rec_name = "player_id"

    player_id = fields.Many2one("goph.golf_player", ondelete="cascade")
    team_id = fields.Many2one("goph.golf_team", ondelete="cascade")
    player_handicap = fields.Float()

    tournament_id = fields.Many2one("goph.tournament", ondelete="cascade")
    tournament_mode = fields.Selection(related="tournament_id.tournament_mode")
    tournament_scoring_format = fields.Selection(related="tournament_id.tournament_scoring_format")  # for filtering
    player_score_cards = fields.One2many("goph.player_score_card", "registered_player_id")

    main_division_id = fields.Many2one("goph.eligible_division_instance", string="Division")
    player_classification_id = fields.Many2one("goph.classification", compute="_set_classification")
    stored_player_classification_id = fields.Many2one("goph.classification", string="Classification")

    # stored fields
    total_gross_score = fields.Integer(string="Total Gross Score", store=True)
    total_net_score = fields.Integer(string="Total Net Score", store=True)
    player_gross_ranking = fields.Integer(string="Gross Ranking", store=True)
    player_net_ranking = fields.Integer(string="Net Ranking", store=True)
    overall_gross_ranking = fields.Integer(string="Overall Gross Ranking", store=True)
    overall_net_ranking = fields.Integer(string="Overall Net Ranking", store=True)

    @api.multi
    @api.depends('player_handicap')
    def _set_classification(self):
        for record in self:
            if record.tournament_mode == 'team-mode':
                record.player_classification_id = record.team_id.stored_team_classification_id.id
                record.write({'stored_player_classification_id' : record.team_id.stored_team_classification_id.id}) 
            else:
                classifications = record.main_division_id.classifications
                for classification in classifications:
                    if ((record.player_handicap >= classification.classification_min_handicap) and 
                            (record.player_handicap <= classification.classification_max_handicap)):
                        record.player_classification_id = classification.id
                        record.write({'stored_player_classification_id' : classification.id})
                        break
    
    def update_main_division_rankings(self, registered_player):

        same_main_division_players = registered_player.main_division_id.qualified_players
        players_with_zero_gross = same_main_division_players.filtered(lambda x : x.total_gross_score == 0)
        players_with_gross_score = same_main_division_players.filtered(lambda x : x.total_gross_score != 0)

        for player in same_main_division_players:
            
            vals = {}
            
            if player.total_gross_score == 0:
                sorted_zero_gross = players_with_zero_gross.sorted('id').ids
                vals['overall_gross_ranking'] = sorted_zero_gross.index(player.id) + 1 + len(players_with_gross_score)
                vals['overall_net_ranking'] = vals['overall_gross_ranking']
            else:
                if (registered_player.tournament_id.tournament_scoring_format == 'system-36' or 
                        registered_player.tournament_id.tournament_scoring_format == 'stroke-play'):
                    sorted_by_gross_overall = players_with_gross_score.sorted('total_gross_score').ids
                    sorted_by_net_overall = players_with_gross_score.sorted('total_net_score').ids
                else:
                    sorted_by_gross_overall = players_with_gross_score.sorted('total_gross_score', reverse=True).ids
                    sorted_by_net_overall = players_with_gross_score.sorted('total_net_score', reverse=True).ids

                vals['overall_gross_ranking'] = sorted_by_gross_overall.index(player.id) + 1
                vals['overall_net_ranking'] = sorted_by_net_overall.index(player.id) + 1

            player.write(vals)
        
        # update classification rankings if tournament format is not system-36
        if registered_player.tournament_id.tournament_scoring_format != 'system-36':
            registered_player.update_classification_rankings(registered_player)

    def update_classification_rankings(self, registered_player):
        
        same_classification_players = registered_player.stored_player_classification_id.qualified_players
        players_with_zero_gross = same_classification_players.filtered(lambda x: x.total_gross_score == 0)
        players_with_gross_score = same_classification_players.filtered(lambda x: x.total_gross_score != 0)

        for player in same_classification_players:

            vals = {}

            if player.total_gross_score == 0:
                sorted_zero_gross = players_with_zero_gross.sorted('id').ids
                vals['player_gross_ranking'] = sorted_zero_gross.index(player.id) + 1 + len(players_with_gross_score)
                vals['player_net_ranking'] = vals['player_gross_ranking']
            else:
                if (registered_player.tournament_id.tournament_scoring_format == 'system-36' or 
                        registered_player.tournament_id.tournament_scoring_format == 'stroke-play'):
                    sorted_by_gross = players_with_gross_score.sorted('total_gross_score').ids
                    sorted_by_net = players_with_gross_score.sorted('total_net_score').ids
                else:        
                    sorted_by_gross = players_with_gross_score.sorted('total_gross_score', reverse=True).ids
                    sorted_by_net = players_with_gross_score.sorted('total_net_score', reverse=True).ids
                
                vals['player_gross_ranking'] = sorted_by_gross.index(player.id) + 1
                vals['player_net_ranking'] = sorted_by_net.index(player.id) + 1

            player.write(vals)
    
    def set_classification_id(self, registered_player):
        if registered_player.tournament_mode == 'team-mode':
            registered_player.player_classification_id = registered_player.team_id.stored_team_classification_id.id
            registered_player.write({'stored_player_classification_id' : registered_player.team_id.stored_team_classification_id.id}) 
        else:
            classifications = registered_player.main_division_id.classifications
            for classification in classifications:
                if ((registered_player.player_handicap >= classification.classification_min_handicap) and 
                        (registered_player.player_handicap <= classification.classification_max_handicap)):
                    registered_player.player_classification_id = classification.id
                    registered_player.write({'stored_player_classification_id' : classification.id})
                    break
    
    @api.multi
    def write(self, vals):
        for record in self:
            # getting the old division and classification
            old_main_division = record.main_division_id
            old_classification = record.stored_player_classification_id

            # writing the updated main division (classification not updated yet)
            super(RegisteredPlayer, record).write(vals)
            
            # assigning the updated main division
            new_main_division = record.main_division_id
            # re-computing classification by calling 'player_classification_id'
            new_classification = record.player_classification_id

            # if main division has been updated, update awardees
            if old_main_division.id != new_main_division.id:
                old_main_division.update_awardees(old_main_division)
                new_main_division.update_awardees(new_main_division)
                old_classification.update_awardees(old_classification)
                new_classification.update_awardees(new_classification)

        return True
