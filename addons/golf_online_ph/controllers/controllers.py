# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import odoo.addons.website.controllers.main as website_controller
import datetime

class ExtendedWebsiteController(website_controller.Website):
    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        Tournaments = http.request.env['goph.tournament']
        return http.request.render('golf_online_ph.homepage', {
            'home_is_active': 'active',
            'recent_tournaments': Tournaments.search([]).filtered(lambda r: r.tournament_status == 'Recent').sorted('tournament_end_sched', reverse=True),
            'ongoing_tournaments': Tournaments.search([]).filtered(lambda r: r.tournament_status == 'Ongoing').sorted('tournament_start_sched'),
            'upcoming_tournaments': Tournaments.search([]).filtered(lambda r: r.tournament_status == 'Upcoming').sorted('tournament_start_sched'),
        })

class GolfOnlinePh(http.Controller):
    @http.route('/home/', type="http", auth='public', website=True)
    def index(self, **kw):
        homepage = request.website.homepage_id
        if homepage and (homepage.sudo().is_visible or request.env.user.has_group('base.group_user')) and homepage.url != '/':
            return request.env['ir.http'].reroute(homepage.url)

        website_page = request.env['ir.http']._serve_page()
        if website_page:
            return website_page
        else:
            top_menu = request.website.menu_id
            first_menu = top_menu and top_menu.child_id and top_menu.child_id.filtered(
                lambda menu: menu.is_visible)
            if first_menu and first_menu[0].url not in ('/', '') and (not (first_menu[0].url.startswith(('/?', '/#', ' ')))):
                return request.redirect(first_menu[0].url)

        raise request.not_found()

    @http.route('/about/', auth='public', website=True)
    def about(self):
        return http.request.render('golf_online_ph.about', {
            'about_is_active' : 'active',
        })
    

    @http.route(['/tournaments/'], auth='public', website=True)
    def _tournaments_page(self, **post):
        Tournaments = http.request.env['goph.tournament']
        return http.request.render('golf_online_ph.tournaments_page', {
            'tournament_is_active': 'active',
            'tournaments': Tournaments.search([])
        })

    @http.route('/tournaments/<model("goph.tournament"):tournament>', auth='public', website=True)
    def _tournament_details(self, tournament):
        return http.request.render('golf_online_ph.tournament_details', {
            'tournament_is_active': 'active',
            'tournament': tournament
        })

    @http.route(['/courses/'], auth='public', website=True)
    def _course_page(self, **post):
        Courses = http.request.env['goph.course']
        return http.request.render('golf_online_ph.course_page', {
            'course_is_active': 'active',
            'courses': Courses.search([])
        })

    @http.route('/courses/<model("goph.course"):course>', auth='public', website=True)
    def _course_details(self, course):
        return http.request.render('golf_online_ph.course_details', {
            'course_is_active': 'active',
            'course': course
        })

    @http.route('/teams/<model("goph.golf_team"):team>', auth='public', website=True)
    def _teams_page(self, team):
        return http.request.render('golf_online_ph.teams_page', {
            'tournament_is_active': 'active',
            'team': team,
            'tournament': team.tournament_id
        })

    @http.route('/players/<model("goph.registered_player"):player>', auth='public', website=True)
    def _players_page(self, player):
        return http.request.render('golf_online_ph.players_page', {
            'tournament_is_active': 'active',
            'player': player,
            'tournament': player.tournament_id 
        })
