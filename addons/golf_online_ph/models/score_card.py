from odoo import models, fields, api


class PlayerScoreCard(models.Model):
    _name = "goph.player_score_card"
    _rec_name = "registered_player_id"

    registered_player_id = fields.Many2one("goph.registered_player", ondelete="cascade")
    team_id = fields.Many2one(related="registered_player_id.team_id")
    team_round_id = fields.Many2one("goph.team_rounds")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    player_main_division = fields.Many2one(related="registered_player_id.main_division_id")
    player_classification = fields.Many2one(related="registered_player_id.player_classification_id")

    tournament_id = fields.Many2one("goph.tournament")
    tournament_scoring_format = fields.Selection(related="tournament_id.tournament_scoring_format")
    tournament_mode = fields.Selection(related="tournament_id.tournament_mode")
    round_id = fields.Many2one("goph.tournament_rounds", ondelete="cascade")

    front_nine_default_table = fields.One2many("goph.front_nine_default", "player_score_card_id")
    back_nine_default_table = fields.One2many("goph.back_nine_default", "player_score_card_id")
    front_nine_system_36_table = fields.One2many("goph.front_nine_system_36", "player_score_card_id")
    back_nine_system_36_table = fields.One2many("goph.back_nine_system_36", "player_score_card_id")
    front_nine_stroke_play_table = fields.One2many("goph.front_nine_stroke_play", "player_score_card_id")
    back_nine_stroke_play_table = fields.One2many("goph.back_nine_stroke_play", "player_score_card_id")

    # FRONT NINE FIELDS
    front_nine_hole_par = fields.Integer(compute="_get_front_nine_hole_par", string="Front Nine Hole Par")
    front_nine_converted_par = fields.Integer(compute="_get_front_nine_cnvrtd_hole_par", string="Front Nine Converted Hole Par")
    front_nine_to_par = fields.Integer(compute="_get_front_nine_to_par", string="Front Nine To Par")
    front_nine_strokes = fields.Integer(compute="_get_front_nine_strokes", string="Front Nine Strokes")
    front_nine_gross = fields.Integer(compute="_get_front_nine_gross", string="Front Nine Gross")
    front_nine_net = fields.Integer(compute="_get_front_nine_net", string="Front Nine Net")
    front_nine_accrued = fields.Integer(compute="_get_front_nine_accrued", string="Front Nine Reduction")


    # BACK NINE FIELDS
    back_nine_hole_par = fields.Integer(compute="_get_back_nine_hole_par", string="Back Nine Hole Par")
    back_nine_converted_par = fields.Integer(compute="_get_back_nine_cnvrtd_hole_par", string="Back Nine Converted Hole Par")
    back_nine_to_par = fields.Integer(compute="_get_back_nine_to_par", string="Back Nine To Par")
    back_nine_strokes = fields.Integer(compute="_get_back_nine_strokes", string="Back Nine Strokes")
    back_nine_gross = fields.Integer(compute="_get_back_nine_gross", string="Back Nine Strokes")
    back_nine_net = fields.Integer(compute="_get_back_nine_net", string="Back Nine Net")
    back_nine_accrued = fields.Integer(compute="_get_back_nine_accrued", string="Back Nine Reduction")

    # SUMMARY (non stored)
    summary_hole_par = fields.Integer(compute="_get_total_hole_par", string="Total Hole Par")
    summary_converted_par = fields.Integer(compute="_get_total_converted_par", string="Total Converted Par")
    summary_to_par = fields.Integer(compute="_get_total_to_par", string="Total To Par")
    summary_total_strokes = fields.Integer(compute="_get_total_strokes", string="Total Strokes")
    summary_gross_points = fields.Integer(compute="_get_total_gross_points", string="Total Gross Points")
    summary_net_points = fields.Integer(compute="_get_total_net_points", string="Total Net Points")
    summary_accrued_points = fields.Integer(compute="_get_total_accrued_points", string="Total Accrued Points")

    # SUMMARY (stored)
    stored_field_holder = fields.Integer(compute="_compute_stored_fields")
    stored_summary_gross_points = fields.Integer(store=True, string="Total Gross")
    stored_summary_net_points = fields.Integer(store=True, string="Total Net")

    penalty = fields.Float(default=0)
    penalty_description = fields.Char()

    @api.multi
    def _get_front_nine_hole_par(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.front_nine_hole_par = sum(rec.hole_par for rec in record.front_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.front_nine_hole_par = sum(rec.hole_par for rec in record.front_nine_stroke_play_table)
            else:
                record.front_nine_hole_par = sum(rec.hole_par for rec in record.front_nine_default_table)


    @api.multi
    def _get_back_nine_hole_par(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.back_nine_hole_par = sum(rec.hole_par for rec in record.back_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.back_nine_hole_par = sum(rec.hole_par for rec in record.back_nine_stroke_play_table)
            else:
                record.back_nine_hole_par = sum(rec.hole_par for rec in record.back_nine_default_table)

    @api.multi
    @api.depends('front_nine_default_table')
    def _get_front_nine_cnvrtd_hole_par(self):
        for record in self:
            record.front_nine_converted_par = sum(rec.converted_par_value for rec in record.front_nine_default_table)

    @api.multi
    @api.depends('back_nine_default_table')
    def _get_back_nine_cnvrtd_hole_par(self):
        for record in self:
            record.back_nine_converted_par = sum(rec.converted_par_value for rec in record.back_nine_default_table)

    @api.multi
    @api.depends('front_nine_system_36_table', 'front_nine_stroke_play_table', 'front_nine_default_table')
    def _get_front_nine_to_par(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.front_nine_to_par = sum(rec.table_to_par for rec in record.front_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.front_nine_to_par = sum(rec.table_to_par for rec in record.front_nine_stroke_play_table)
            else:
                record.front_nine_to_par = sum(rec.table_to_par for rec in record.front_nine_default_table)

    @api.multi
    @api.depends('back_nine_system_36_table', 'back_nine_stroke_play_table', 'back_nine_default_table')
    def _get_back_nine_to_par(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.back_nine_to_par = sum(rec.table_to_par for rec in record.back_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.back_nine_to_par = sum(rec.table_to_par for rec in record.back_nine_stroke_play_table)
            else:
                record.back_nine_to_par = sum(rec.table_to_par for rec in record.back_nine_default_table)

    @api.multi
    def _get_front_nine_strokes(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.front_nine_strokes = sum(rec.stroke for rec in record.front_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.front_nine_strokes = sum(rec.stroke for rec in record.front_nine_stroke_play_table)
            else:
                record.front_nine_strokes = sum(rec.stroke for rec in record.front_nine_default_table)

    @api.multi
    def _get_back_nine_strokes(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                record.back_nine_strokes = sum(rec.stroke for rec in record.back_nine_system_36_table)
            elif record.tournament_scoring_format == 'stroke-play':
                record.back_nine_strokes = sum(rec.stroke for rec in record.back_nine_stroke_play_table)
            else:
                record.back_nine_strokes = sum(rec.stroke for rec in record.back_nine_default_table)


    @api.multi
    @api.depends('front_nine_default_table')
    def _get_front_nine_gross(self):
        for record in self:
            record.front_nine_gross = sum(rec.table_gross_points for rec in record.front_nine_default_table)

    @api.multi
    @api.depends('front_nine_default_table')
    def _get_front_nine_net(self):
        for record in self:
            record.front_nine_net = sum(rec.table_net_points for rec in record.front_nine_default_table)

    @api.multi
    @api.depends('front_nine_system_36_table')
    def _get_front_nine_accrued(self):
        for record in self:
            record.front_nine_accrued = sum(rec.table_accrued_points for rec in record.front_nine_system_36_table)

    @api.multi
    @api.depends('back_nine_default_table')
    def _get_back_nine_gross(self):
        for record in self:
            record.back_nine_gross = sum(rec.table_gross_points for rec in record.back_nine_default_table)

    @api.multi
    @api.depends('back_nine_default_table')
    def _get_back_nine_net(self):
        for record in self:
            record.back_nine_net = sum(rec.table_net_points for rec in record.back_nine_default_table)

    @api.multi
    @api.depends('back_nine_system_36_table')
    def _get_back_nine_accrued(self):
        for record in self:
            record.back_nine_accrued = sum(rec.table_accrued_points for rec in record.back_nine_system_36_table)

    @api.multi
    def _get_total_hole_par(self):
        for record in self:
            record.summary_hole_par = record.front_nine_hole_par + record.back_nine_hole_par

    @api.multi
    @api.depends('front_nine_converted_par', 'back_nine_converted_par')
    def _get_total_converted_par(self):
        for record in self:
            record.summary_converted_par = record.front_nine_converted_par + record.back_nine_converted_par

    @api.multi
    @api.depends('front_nine_to_par', 'back_nine_to_par')
    def _get_total_to_par(self):
        for record in self:
            record.summary_to_par = record.front_nine_to_par + record.back_nine_to_par

    @api.multi
    def _get_total_strokes(self):
        for record in self:
            record.summary_total_strokes = record.front_nine_strokes + record.back_nine_strokes

    @api.multi
    @api.depends('front_nine_gross', 'back_nine_gross')
    def _get_total_gross_points(self):
        for record in self:
            if (record.tournament_scoring_format == 'system-36' or
                    record.tournament_scoring_format == 'stroke-play'):
                record.summary_gross_points = (record.front_nine_strokes + record.back_nine_strokes) + record.penalty
            else:
                record.summary_gross_points = (record.front_nine_gross + record.back_nine_gross) - record.penalty

    @api.multi
    @api.depends('front_nine_net', 'back_nine_net', 'summary_accrued_points', 'summary_gross_points')
    @api.depends('player_handicap')
    def _get_total_net_points(self):
        for record in self:
            if record.tournament_scoring_format == 'system-36':
                gross_score = record.summary_gross_points
                if gross_score == 0:
                    record.summary_net_points = 0
                else:
                    record.summary_net_points = (gross_score) - (36 - record.summary_accrued_points)
            elif record.tournament_scoring_format == 'stroke-play':
                gross_score = record.summary_gross_points
                if gross_score == 0:
                    record.summary_net_points = 0
                else:
                    record.summary_net_points = gross_score - record.player_handicap
            else:
                record.summary_net_points = (record.front_nine_net + record.back_nine_net) - record.penalty

    @api.multi
    def _get_total_accrued_points(self):
        for record in self:
            record.summary_accrued_points = (record.front_nine_accrued + record.back_nine_accrued)

    @api.multi
    @api.depends('summary_gross_points', 'summary_net_points')
    def _compute_stored_fields(self):
        for record in self:
            if record.summary_total_strokes != 0:
                vals = {}

                vals['stored_summary_gross_points'] = record.summary_gross_points
                vals['stored_summary_net_points'] = record.summary_net_points

                record.write(vals)  # update score card gross and net points
                # update registered player total gross and total net
                record.update_player_record(record.registered_player_id, record)

    def update_player_record(self, registered_player, score_card):
        vals = {}

        temp_gross_score = 0
        temp_net_score = 0

        # update player score
        for score_record in registered_player.player_score_cards:
            temp_gross_score += score_record.stored_summary_gross_points
            temp_net_score += score_record.stored_summary_net_points

        vals['total_gross_score'] = temp_gross_score
        vals['total_net_score'] = temp_net_score

        # update player handicap if tournament scoring format is system 36
        if registered_player.tournament_id.tournament_scoring_format == 'system-36':
            temp_player_handicap = 36 - score_card.summary_accrued_points
            if temp_player_handicap < 0:
                temp_player_handicap = 0

            vals['player_handicap'] = temp_player_handicap

            golf_player = self.env['goph.golf_player'].browse(registered_player.player_id.id)
            golf_player.write({'player_handicap': temp_player_handicap})

        registered_player.write(vals)  # update registered player records with vals
        # call to update main division rankings
        registered_player.update_main_division_rankings(registered_player)

        # update team score if tournament format is team mode
        if registered_player.tournament_id.tournament_mode == 'team-mode':
            team_round = self.env['goph.team_rounds'].search([
                ('team_id', '=', registered_player.team_id.id),
                ('round_id', '=', score_card.round_id.id )
            ])
            team_round.update_round_score(registered_player.team_id, score_card.round_id)

        # update main division awardees
        main_division_obj = registered_player.main_division_id
        main_division_obj.update_awardees(main_division_obj)

        # update classification awardees if tournament scoring format is not system 36
        if registered_player.tournament_id.tournament_scoring_format != 'system-36':
            classification_obj = registered_player.stored_player_classification_id
            classification_obj.update_awardees(classification_obj)


class FirstHalfScoreTable(models.Model):
    _name = "goph.front_nine_default"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    # computed fields
    converted_par_value = fields.Integer(string="Par + Hndcp", compute="_compute_converted_par")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_gross_points = fields.Integer(compute="_compute_gross", string="Gross")
    table_net_points = fields.Integer(compute="_compute_net", string="Net")
    table_point_rep = fields.Char(compute="_compute_point_rep")

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                to_par_net = record.stroke - record.converted_par_value
                to_par_gross = record.stroke - record.hole_par
                record.table_to_par = to_par_net

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_gross_points = point_system.filtered(lambda r: r.value == to_par_gross)
                equiv_net_points = point_system.filtered(lambda r: r.value == to_par_net)

                if equiv_net_points.points == 0:
                    if to_par_net > 0:
                        record.table_to_par = 0
                        record.table_char_to_par = "X"
                    else:
                        record.table_char_to_par = str(to_par_net)
                else:
                    if to_par_net == 0:
                        record.table_char_to_par = "E"
                    else:
                        record.table_char_to_par = str(to_par_net)

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_gross(self):
        for record in self:
            if record.stroke == 0 or record.table_net_points == 0 :
                record.table_gross_points = 0
            else:
                to_par_gross = record.stroke - record.hole_par

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_gross_points = point_system.filtered(lambda r: r.value == to_par_gross)

                if equiv_gross_points.points == 0:
                    if to_par_gross > 0:
                        record.table_gross_points = 0
                    else:
                        record.table_gross_points = max(r.points for r in point_system)
                else:
                    record.table_gross_points = equiv_gross_points.points

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_net(self):
        for record in self:
            if record.stroke == 0:
                record.table_net_points = 0
            else:
                to_par_net = record.stroke - record.converted_par_value

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_net_points = point_system.filtered(lambda r: r.value == to_par_net)

                if equiv_net_points.points == 0:  # if value is not found in the point system
                    if to_par_net > 0:  # if to_par_net is a positive, it means over par, not counted
                        record.table_net_points = 0
                    else:  # if to_par_net is negative, it means under par, hole-in-one
                        record.table_net_points = max(r.points for r in point_system)  # assigning the max points from point system
                else:  # if value is found in the point system
                    record.table_net_points = equiv_net_points.points

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"

    @api.multi
    @api.depends("player_handicap")
    def _compute_converted_par(self):
        for record in self:
            if record.tournament_scoring_format != 'system-36':
                player_handicap = int(record.player_handicap)
                num_of_holes = record.player_score_card_id.tournament_id.tournament_course.course_num_of_holes
                # import pdb; pdb.set_trace()
                add_to_all_count = 0
                to_add_to_hole = 0
                while player_handicap > 0:
                    add_to_all_count = int(player_handicap / num_of_holes)
                    if add_to_all_count != 0:
                        to_add_to_hole += add_to_all_count
                        player_handicap = player_handicap - (num_of_holes * add_to_all_count)
                    else:
                        if record.hole_handicap in range (1, (player_handicap+1)):
                            to_add_to_hole += 1
                            break
                        else:
                            break

                record.converted_par_value = record.hole_par + to_add_to_hole

class SecondHalfScoreTable(models.Model):
    _name = "goph.back_nine_default"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    # computed fields
    converted_par_value = fields.Integer(string="Par + Hndcp", compute="_compute_converted_par")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_gross_points = fields.Integer(compute="_compute_gross", string="Gross")
    table_net_points = fields.Integer(compute="_compute_net", string="Net")
    table_point_rep = fields.Char(compute="_compute_point_rep")

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                to_par_net = record.stroke - record.converted_par_value
                to_par_gross = record.stroke - record.hole_par
                record.table_to_par = to_par_net

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_gross_points = point_system.filtered(lambda r: r.value == to_par_gross)
                equiv_net_points = point_system.filtered(lambda r: r.value == to_par_net)

                if equiv_net_points.points == 0:
                    if to_par_net > 0:
                        record.table_to_par = 0
                        record.table_char_to_par = "X"
                    else:
                        record.table_char_to_par = str(to_par_net)
                else:
                    if to_par_net == 0:
                        record.table_char_to_par = "E"
                    else:
                        record.table_char_to_par = str(to_par_net)

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_gross(self):
        for record in self:
            if record.stroke == 0 or record.table_net_points == 0:
                record.table_gross_points = 0
            else:
                to_par_gross = record.stroke - record.hole_par

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_gross_points = point_system.filtered(lambda r: r.value == to_par_gross)

                if equiv_gross_points.points == 0:
                    if to_par_gross > 0:
                        record.table_gross_points = 0
                    else:
                        record.table_gross_points = max(r.points for r in point_system)
                else:
                    record.table_gross_points = equiv_gross_points.points

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id', 'converted_par_value')
    def _compute_net(self):
        for record in self:
            if record.stroke == 0:
                record.table_net_points = 0
            else:
                to_par_net = record.stroke - record.converted_par_value

                point_system = record.registered_player_id.main_division_id.point_system_instances
                equiv_net_points = point_system.filtered(lambda r: r.value == to_par_net)

                if equiv_net_points.points == 0:  # if value is not found in the point system
                    if to_par_net > 0:  # if to_par_net is a positive, it means over par, not counted
                        record.table_net_points = 0
                    else:  # if to_par_net is negative, it means under par, hole-in-one
                        record.table_net_points = max(r.points for r in point_system)  # assigning the max points from point system
                else:  # if value is found in the point system
                    record.table_net_points = equiv_net_points.points

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_char_to_par', 'table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"

    @api.multi
    @api.depends("player_handicap")
    def _compute_converted_par(self):
        for record in self:
            if record.tournament_scoring_format != 'system-36':
                player_handicap = int(record.player_handicap)
                num_of_holes = record.player_score_card_id.tournament_id.tournament_course.course_num_of_holes
                # import pdb; pdb.set_trace()
                add_to_all_count = 0
                to_add_to_hole = 0
                while player_handicap > 0:
                    add_to_all_count = int(player_handicap / num_of_holes)
                    if add_to_all_count != 0:
                        to_add_to_hole += add_to_all_count
                        player_handicap = player_handicap - (num_of_holes * add_to_all_count)
                    else:
                        if record.hole_handicap in range (1, (player_handicap+1)):
                            to_add_to_hole += 1
                            break
                        else:
                            break

                record.converted_par_value = record.hole_par + to_add_to_hole


class FrontNineSystem36(models.Model):
    _name = "goph.front_nine_system_36"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_accrued_points = fields.Integer(compute="_compute_accrued", string="Accrued")
    table_point_rep = fields.Char(compute="_compute_point_rep")


    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                temp_to_par = record.stroke - record.hole_par
                record.table_to_par = temp_to_par
                if temp_to_par == 0:
                    record.table_char_to_par = "E"
                else:
                    record.table_char_to_par = str(temp_to_par)

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id')
    def _compute_accrued(self):
        for record in self:
            if record.stroke == 0:
                record.table_accrued_points = 0
            else:
                temp_to_par = record.stroke - record.hole_par

                if temp_to_par <= 0:  # under par or even
                    record.table_accrued_points = 2
                elif temp_to_par == 1:  # bogey
                    record.table_accrued_points = 1
                else:  # double or triple bogey
                    record.table_accrued_points = 0

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"


class BackNineSystem36(models.Model):
    _name = "goph.back_nine_system_36"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_accrued_points = fields.Integer(compute="_compute_accrued", string="Accrued")
    table_point_rep = fields.Char(compute="_compute_point_rep")


    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                temp_to_par = record.stroke - record.hole_par
                record.table_to_par = temp_to_par
                if temp_to_par == 0:
                    record.table_char_to_par = "E"
                else:
                    record.table_char_to_par = str(temp_to_par)

    @api.multi
    @api.onchange('stroke')
    @api.depends('registered_player_id.main_division_id')
    def _compute_accrued(self):
        for record in self:
            if record.stroke == 0:
                record.table_accrued_points = 0
            else:
                temp_to_par = record.stroke - record.hole_par

                if temp_to_par <= 0:  # under par or even
                    record.table_accrued_points = 2
                elif temp_to_par == 1:  # bogey
                    record.table_accrued_points = 1
                else:  # double or triple bogey
                    record.table_accrued_points = 0

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"


class FrontNineStrokePlay(models.Model):
    _name = "goph.front_nine_stroke_play"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_point_rep = fields.Char(compute="_compute_point_rep")

    @api.multi
    @api.onchange('stroke')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                temp_to_par = record.stroke - record.hole_par
                record.table_to_par = temp_to_par
                if temp_to_par == 0:
                    record.table_char_to_par = "E"
                else:
                    record.table_char_to_par = str(temp_to_par)

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"


class BackNineStrokePlay(models.Model):
    _name = "goph.back_nine_stroke_play"

    player_score_card_id = fields.Many2one("goph.player_score_card", ondelete="cascade")
    hole_id = fields.Many2one("goph.course_holes", string="Hole #")
    stroke = fields.Integer()

    # related fields
    registered_player_id = fields.Many2one(related="player_score_card_id.registered_player_id")
    player_handicap = fields.Float(related="registered_player_id.player_handicap")
    hole_par = fields.Integer(related="hole_id.hole_par", string="Hole Par")
    hole_handicap = fields.Integer(related="hole_id.hole_handicap", string="Hole Handicap")  # please set it to store = True in the future so it can be sorted in the table
    tournament_scoring_format = fields.Selection(related="player_score_card_id.tournament_id.tournament_scoring_format", string="Scoring Format")

    table_to_par = fields.Integer(compute="_compute_to_par")
    table_char_to_par = fields.Char(compute="_compute_to_par", string="To Par")
    table_point_rep = fields.Char(compute="_compute_point_rep")

    @api.multi
    @api.onchange('stroke')
    def _compute_to_par(self):
        for record in self:
            if record.stroke == 0:
                record.table_to_par = 0
                record.table_char_to_par = "X"
            else:
                temp_to_par = record.stroke - record.hole_par
                record.table_to_par = temp_to_par
                if temp_to_par == 0:
                    record.table_char_to_par = "E"
                else:
                    record.table_char_to_par = str(temp_to_par)

    @api.multi
    @api.onchange('stroke')
    @api.depends('table_to_par')
    def _compute_point_rep(self):
        for record in self:

            temp_to_par = record.table_to_par

            if temp_to_par <= -4:
                record.table_point_rep = "hole-in-one"
            elif temp_to_par == -3:
                record.table_point_rep = "double-eagle"
            elif temp_to_par == -2:
                record.table_point_rep = "eagle"
            elif temp_to_par == -1:
                record.table_point_rep = "birdie"
            elif record.table_char_to_par == 'E':
                record.table_point_rep = "par"
            elif temp_to_par == 1:
                record.table_point_rep = "bogey"
            elif temp_to_par == 2:
                record.table_point_rep = "double-bogey"
            else:
                record.table_point_rep = "not-counted"
