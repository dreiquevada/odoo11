$(document).ready(function() {
    
    $("#tournament-list-table").DataTable({
        "order": [[0, "desc"]]
    });
    
    $("#courses-list-table").DataTable({
        "order": [[0, "desc"]]
    });

    $("table[id^='division-table-']").DataTable();

    $("#team-members").DataTable({
        "order": [[0, "asc"]]
    });

    var groupColumn = 7;
    var team_table = $("table[id^='division-team-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[5, "asc"]],
        orderFixed: [[7, "asc"]],
        columnDefs: [
            { "visible": false, "targets": groupColumn }
        ],
        displayLength: 25,
        rowGroup: {
            dataSrc: 7
        },
    });
    
    // Order by the grouping
    $("table[id^='division-team-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = team_table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            team_table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            team_table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );

    var individual_table = $("table[id^='division-individual-table-']").DataTable({
        // ordered by gross ranking
        pageLength: 25,
        order: [[5, "asc"]],
        orderFixed: [[7, "asc"]],
        columnDefs: [
            { "visible": false, "targets": groupColumn }
        ],
        displayLength: 25,
        rowGroup: {
            dataSrc: 7
        },
    });
    
    // Order by the grouping
    $("table[id^='division-individual-table-'] tbody").on( 'click', 'tr.group', function () {
        var currentOrder = individual_table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            individual_table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            individual_table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );


    $("table[id^='system-36-team-table-']").DataTable({
        "order": [[5, "asc"]]
    });

    $("table[id^='system-36-individual-table-']").DataTable({
        "order": [[5, "asc"]]
    });

    $("table[id^='flight-table-']").DataTable({
    });

    $("#team-members").DataTable({
        "order": [[0, "asc"]]
    });

    $("#system-36-table").DataTable({
        "order": [[0, "asc"]]
    });

    $('#main-navigation nav li').click(function(){
        $('li').removeClass("active");
        $(this).addClass("active");
    });
    // legend pop-over
    $(function(){
        // Enables popover
        $("[data-toggle=popover]").popover();
    });
});