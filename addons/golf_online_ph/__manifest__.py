# -*- coding: utf-8 -*-
{
    'name': "Golf Online PH",

    'summary': """
        See your scores online.""",

    'description': """
        This app let's you see your scores online and manage golf tournaments efficiently. You
        can also view your favorite player's tournament history and team history, as well as 
        the top golf courses.
    """,

    'author': "Golf Online",
    'website': "http://www.golfphonline.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Management',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'website',
    ],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'demo/default_data.xml',
        'views/backend/course.xml',
        'views/backend/tournament.xml',
        'views/backend/tournament_format.xml',
        'views/backend/team.xml',
        'views/backend/rounds.xml',
        'views/backend/score_card.xml',
        'views/backend/player.xml',
        'views/backend/flights.xml',
        'views/backend/registered_players_teams.xml',
        'views/frontend/templates.xml',
        'views/frontend/tournament_page.xml',
        'views/frontend/course_page.xml',
        'views/frontend/teams_page.xml',
        'views/frontend/players_page.xml',
        'views/frontend/home_page.xml',

        # menuitems should always be last
        'views/backend/menuitems.xml',
    ],
    'application': True,
}
